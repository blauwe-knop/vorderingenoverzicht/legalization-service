package model

type Legalization struct {
	Token        string   `json:"token"`
	AppPublicKey string   `json:"appPublicKey"`
	Bsn          string   `json:"bsn"`
	CreatedAt    JSONTime `json:"createdAt"`
	ExpiresAt    JSONTime `json:"expiresAt"`
}
