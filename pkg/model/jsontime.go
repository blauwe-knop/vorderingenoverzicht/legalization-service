package model

import (
	"fmt"
	"strings"
	"time"
)

type JSONTime time.Time

func (t JSONTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", time.Time(t).Format(time.RFC3339))
	return []byte(stamp), nil
}

func (t *JSONTime) UnmarshalJSON(data []byte) error {
	value := strings.Trim(string(data), `"`) //get rid of "
	if value == "" || value == "null" {
		return nil
	}

	time, err := time.Parse(time.RFC3339, value) //parse time
	if err != nil {
		return err
	}
	*t = JSONTime(time) //set result using the pointer
	return nil
}
