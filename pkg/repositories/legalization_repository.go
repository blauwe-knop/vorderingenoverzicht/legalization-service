// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/model"
)

type LegalizationRepository interface {
	List() (*[]model.Legalization, error)
	Create(Legalization model.Legalization) (*model.Legalization, error)
	Get(id string) (*model.Legalization, error)
	Update(id string, Legalization model.Legalization) (*model.Legalization, error)
	Delete(id string) (*model.Legalization, error)
	healthcheck.Checker
}
