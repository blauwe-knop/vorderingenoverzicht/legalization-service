// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/model"
)

type LegalizationClient struct {
	baseURL string
	apiKey  string
}

var ErrLegalizationNotFound = errors.New("legalization does not exist")

func NewLegalizationClient(baseURL string, apiKey string) *LegalizationClient {
	return &LegalizationClient{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *LegalizationClient) List() (*[]model.Legalization, error) {
	url := fmt.Sprintf("%s/legalizations/", s.baseURL)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get legalizations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get legalization list: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving legalization list: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	legalizations := []model.Legalization{}
	err = json.Unmarshal(body, &legalizations)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &legalizations, nil
}

func (s *LegalizationClient) Create(legalization model.Legalization) (*model.Legalization, error) {
	url := fmt.Sprintf("%s/legalizations/", s.baseURL)

	requestBodyAsJson, err := json.Marshal(legalization)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create post legalization request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to post legalization: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while creating legalization: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnLegalization := model.Legalization{}
	err = json.Unmarshal(body, &returnLegalization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnLegalization, nil
}

func (s *LegalizationClient) Get(id string) (*model.Legalization, error) {
	if id == "" {
		return nil, fmt.Errorf("failed to get legalization, invalid id")
	}

	url := fmt.Sprintf("%s/legalizations/%s", s.baseURL, id)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get legalization request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get legalization: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrLegalizationNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving legalization: %d, id: %s", resp.StatusCode, id)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	legalization := model.Legalization{}
	err = json.Unmarshal(body, &legalization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json, id: %s: %v", id, err)
	}

	return &legalization, nil
}

func (s *LegalizationClient) Update(id string, legalization model.Legalization) (*model.Legalization, error) {
	url := fmt.Sprintf("%s/legalizations/%s", s.baseURL, id)

	requestBodyAsJson, err := json.Marshal(legalization)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create update legalization request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to update legalization: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while updating legalization: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnLegalization := model.Legalization{}
	err = json.Unmarshal(body, &returnLegalization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnLegalization, nil
}

func (s *LegalizationClient) Delete(id string) (*model.Legalization, error) {
	url := fmt.Sprintf("%s/legalizations/%s", s.baseURL, id)

	request, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create delete legalizations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to delete legalization: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while deleting legalization: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	legalization := model.Legalization{}
	err = json.Unmarshal(body, &legalization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &legalization, nil
}
func (s *LegalizationClient) GetHealthCheck() healthcheck.Result {
	name := "legalization-service"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	body, err := io.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
