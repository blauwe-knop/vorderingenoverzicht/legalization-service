// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/internal/repositories"
)

type options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the legalization-service api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	APIKey        string `long:"api-key" env:"API_KEY" default:"" description:"Key to protect the API endpoints."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	legalizationRepository := repositories.NewLegalizationStore(context.Background(), logger)
	if err != nil {
		log.Fatal("error creating legalization db", zap.Error(err))
	}

	router := http_infra.NewRouter(legalizationRepository, cliOptions.APIKey)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
