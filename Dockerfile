FROM golang:1-alpine AS build

RUN apk add --update --no-cache git

ADD . /go/src/legalization-service/
WORKDIR /go/src/legalization-service
RUN go mod download
RUN go build -o dist/bin/legalization-service ./cmd/legalization-service

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/legalization-service/dist/bin/legalization-service /usr/local/bin/legalization-service
COPY --from=build /go/src/legalization-service/api/openapi.json /api/openapi.json
COPY --from=build /go/src/legalization-service/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/legalization-service"]
