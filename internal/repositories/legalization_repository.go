// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/model"
)

type LegalizationRepository interface {
	CreateLegalization(ctx context.Context, newLegalization model.Legalization) (*model.Legalization, error)
	ListLegalization(ctx context.Context) []model.Legalization
	GetLegalization(ctx context.Context, legalizationToken string) (*model.Legalization, error)
	UpdateLegalization(ctx context.Context, newLegalization model.Legalization, legalizationToken string) (*model.Legalization, error)
	DeleteLegalization(ctx context.Context, legalizationToken string) error
	healthcheck.Checker
}
