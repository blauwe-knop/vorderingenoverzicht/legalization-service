// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/model"
)

type LegalizationStore struct {
	log *zap.Logger

	legalizationList []model.Legalization
}

var ErrLegalizationNotFound = errors.New("legalization does not exist")

func NewLegalizationStore(ctx context.Context, log *zap.Logger) *LegalizationStore {
	return &LegalizationStore{log: log, legalizationList: []model.Legalization{}}
}

func (s *LegalizationStore) CreateLegalization(ctx context.Context, newLegalization model.Legalization) (*model.Legalization, error) {
	s.log.Debug("Create", zap.String("legalization.token", newLegalization.Token))

	s.legalizationList = append(s.legalizationList, newLegalization)

	return &newLegalization, nil
}

func (s *LegalizationStore) ListLegalization(ctx context.Context) []model.Legalization {
	return s.legalizationList
}

func (s *LegalizationStore) GetLegalization(ctx context.Context, legalizationToken string) (*model.Legalization, error) {
	s.log.Debug("Get", zap.String("legalizationToken", fmt.Sprint(legalizationToken)))
	result := []model.Legalization{}

	for _, s := range s.legalizationList {
		if s.Token == legalizationToken {
			result = append(result, s)
			break
		}
	}

	if len(result) == 0 {
		return nil, ErrLegalizationNotFound
	}

	return &result[0], nil
}

func (s *LegalizationStore) UpdateLegalization(ctx context.Context, newLegalization model.Legalization, legalizationToken string) (*model.Legalization, error) {
	s.log.Debug("Update", zap.String("legalizationToken", fmt.Sprint(legalizationToken)))

	found := false
	for i, n := range s.legalizationList {
		if n.Token == legalizationToken {
			s.legalizationList[i].AppPublicKey = newLegalization.AppPublicKey
			s.legalizationList[i].CreatedAt = newLegalization.CreatedAt
			s.legalizationList[i].ExpiresAt = newLegalization.ExpiresAt
			s.legalizationList[i].Token = newLegalization.Token
			s.legalizationList[i].Bsn = newLegalization.Bsn
			found = true
			break
		}
	}

	if !found {
		return nil, ErrLegalizationNotFound
	}

	return &newLegalization, nil
}

func (s *LegalizationStore) DeleteLegalization(ctx context.Context, legalizationToken string) error {
	s.log.Debug("Delete", zap.String("legalizationToken", fmt.Sprint(legalizationToken)))

	var foundIndex *int
	for i, n := range s.legalizationList {
		if n.Token == legalizationToken {
			foundIndex = &i
			break
		}
	}

	if foundIndex == nil {
		return ErrLegalizationNotFound
	}

	s.legalizationList = append(s.legalizationList[:*foundIndex], s.legalizationList[*foundIndex+1:]...)

	return nil
}

func (s *LegalizationStore) GetHealthCheck() healthcheck.Result {
	return healthcheck.Result{
		Name:         "legalization-store",
		Status:       healthcheck.StatusOK,
		ResponseTime: 0,
		HealthChecks: []healthcheck.Result{},
	}
}
