// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"log"
	"net/http"
	"os"
)

func handlerJson(w http.ResponseWriter, r *http.Request) {
	fileBytes, err := os.ReadFile("/api/openapi.json")
	if err != nil {
		panic(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(fileBytes)

	if err != nil {
		log.Printf("failed to write fileBytes: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerYaml(w http.ResponseWriter, r *http.Request) {
	fileBytes, err := os.ReadFile("/api/openapi.yaml")
	if err != nil {
		panic(err)
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/octet-stream")
	_, err = w.Write(fileBytes)

	if err != nil {
		log.Printf("failed to write fileBytes: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
