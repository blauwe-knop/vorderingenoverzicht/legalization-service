// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/internal/repositories"
)

type key int

const (
	nonceRepositoryKey        key = iota
	legalizationRepositoryKey key = iota
)

func NewRouter(legalizationRepository repositories.LegalizationRepository, apiKey string) *chi.Mux {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	})

	r.Use(cors.Handler)

	r.Route("/v1", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "1.0.0"))

		r.Route("/legalizations", func(r chi.Router) {
			r.Use(middleware.Logger)

			r.Use(authenticatedOnly(apiKey))

			r.Get("/", func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), legalizationRepositoryKey, legalizationRepository)
				handlerList(w, r.WithContext(ctx))
			})
			r.Post("/", func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), legalizationRepositoryKey, legalizationRepository)
				handlerCreate(w, r.WithContext(ctx))
			})
			r.Get("/{legalizationToken}", func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), legalizationRepositoryKey, legalizationRepository)
				handlerGet(w, r.WithContext(ctx))
			})
			r.Put("/{legalizationToken}", func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), legalizationRepositoryKey, legalizationRepository)
				handlerUpdate(w, r.WithContext(ctx))
			})
			r.Delete("/{legalizationToken}", func(w http.ResponseWriter, r *http.Request) {
				ctx := context.WithValue(r.Context(), legalizationRepositoryKey, legalizationRepository)
				handlerDelete(w, r.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(w http.ResponseWriter, r *http.Request) {
			handlerJson(w, r)
		})

		r.Get("/openapi.yaml", func(w http.ResponseWriter, r *http.Request) {
			handlerYaml(w, r)
		})

		healthCheckHandler := healthcheck.NewHandler("legalization-service", []healthcheck.Checker{legalizationRepository})
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})

	})

	return r
}

func authenticatedOnly(apiKey string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			providedApiKey := r.Header.Get("Authentication")

			if providedApiKey != apiKey {
				log.Printf("Unauthorized: apikey unvalid")
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r)
		})
	}
}
