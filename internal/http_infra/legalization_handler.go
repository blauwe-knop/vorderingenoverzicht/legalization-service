// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/legalization-service/pkg/model"
)

func handlerList(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationRepository, _ := ctx.Value(legalizationRepositoryKey).(repositories.LegalizationRepository)

	legalizations := legalizationRepository.ListLegalization(ctx)

	w.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(legalizations)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerCreate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationRepository, _ := ctx.Value(legalizationRepositoryKey).(repositories.LegalizationRepository)

	var legalization model.Legalization
	err := json.NewDecoder(r.Body).Decode(&legalization)
	if err != nil {
		log.Printf("failed to decode request payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	createdLegalization, err := legalizationRepository.CreateLegalization(ctx, legalization)
	if err != nil {
		log.Printf("error retrieving legalization: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(*createdLegalization)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerGet(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationRepository, _ := ctx.Value(legalizationRepositoryKey).(repositories.LegalizationRepository)

	legalization, err := legalizationRepository.GetLegalization(ctx, chi.URLParam(r, "legalizationToken"))

	if err == repositories.ErrLegalizationNotFound {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	if err != nil {
		log.Printf("error retrieving legalization: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(*legalization)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationRepository, _ := ctx.Value(legalizationRepositoryKey).(repositories.LegalizationRepository)

	var legalization model.Legalization
	err := json.NewDecoder(r.Body).Decode(&legalization)
	if err != nil {
		log.Printf("failed to decode request payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	legalizationToken := chi.URLParam(r, "legalizationToken")

	_, err = legalizationRepository.GetLegalization(ctx, legalizationToken)
	if err != nil {
		log.Printf("failed to get legalization setting")
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	_, err = legalizationRepository.UpdateLegalization(ctx, legalization, legalizationToken)
	if err != nil {
		log.Printf("error updating legalization: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(legalization)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}

func handlerDelete(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	legalizationRepository, _ := ctx.Value(legalizationRepositoryKey).(repositories.LegalizationRepository)

	legalizationToken := chi.URLParam(r, "legalizationToken")

	legalization, err := legalizationRepository.GetLegalization(ctx, legalizationToken)
	if err != nil {
		log.Printf("failed to get legalization setting")
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	err = legalizationRepository.DeleteLegalization(ctx, legalizationToken)
	if err != nil {
		log.Printf("error deleting legalization: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(*legalization)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
