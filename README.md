# legalization-service

Servicecomponent to make CRUD actions available for the legalization in the 'Vorderingenoverzicht' system.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Now start the legalization service:

```sh
go run cmd/legalization-service/main.go
```

By default, the legalization service will run on port `80`.

## Adding mocks

We use [GoMock](https://github.com/golang/mock) to generate mocks.
When you make updates to code for which there are mocks, you should regenerate the mocks.

### Regenerating mocks

```sh
sh scripts/regenerate-gomock-files.sh
```

## Deployment
